function createList(array, parent = document.body) {
  const list = document.createElement('ul');
  parent.appendChild(list);

  array.map(item => {
    const listItem = document.createElement('li');
    list.appendChild(listItem);

    if (Array.isArray(item)) {
      createList(item, listItem);
    } else {
      listItem.innerText = item;
    }
  });

  let count = 3;
  const countdown = setInterval(() => {
    count--;
    console.log(count);
    if (count === 0) {
      clearInterval(countdown);
      parent.innerHTML = '';
    }
  }, 1000);
}

createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);